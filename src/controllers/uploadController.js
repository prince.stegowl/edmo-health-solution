// // src/controllers/uploadController.js
// import { validationResult } from 'express-validator';
// import { uploadFileToS3, deleteFileFromS3, listFilesFromS3 } from '../services/uploadService.js';
// import prisma from '../prisma.js';

// // Upload a new file and store its URL in the extras field
// export const uploadFile = async (req, res) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     return res.status(400).json({ status: 'error', errors: errors.array() });
//   }

//   try {
//     if (!req.file) {
//       return res.status(400).json({ status: 'error', message: 'No file provided' });
//     }

//     const fileUrl = await uploadFileToS3(req.file);
    
//     const { planId } = req.body;
//     if (!planId) {
//       return res.status(400).json({ status: 'error', message: 'No plan ID provided' });
//     }

//     const plan = await prisma.plan.findUnique({ where: { planId: parseInt(planId, 10) } });
//     if (!plan) {
//       return res.status(404).json({ status: 'error', message: 'Plan not found' });
//     }

//     let extras = plan.extras ? JSON.parse(plan.extras) : [];
//     extras.push({ extraImage: fileUrl });

//     await prisma.plan.update({
//       where: { planId: parseInt(planId, 10) },
//       data: { extras: JSON.stringify(extras) },
//     });

//     res.status(200).json({ status: 'success', fileUrl, message: 'File uploaded successfully' });
//   } catch (error) {
//     console.error('Error in uploadFile:', error);
//     res.status(500).json({ status: 'error', message: 'File upload failed' });
//   }
// };

// // Delete a file and remove its URL from the extras field
// export const deleteFile = async (req, res) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     return res.status(400).json({ status: 'error', errors: errors.array() });
//   }

//   try {
//     const { planId, fileKey } = req.body;

//     if (!planId || !fileKey) {
//       return res.status(400).json({ status: 'error', message: 'Plan ID and file key are required' });
//     }

//     await deleteFileFromS3(fileKey);

//     const plan = await prisma.plan.findUnique({ where: { planId: parseInt(planId, 10) } });
//     if (!plan) {
//       return res.status(404).json({ status: 'error', message: 'Plan not found' });
//     }

//     let extras = plan.extras ? JSON.parse(plan.extras) : [];
//     extras = extras.filter(extra => extra.extraImage !== fileKey);

//     await prisma.plan.update({
//       where: { planId: parseInt(planId, 10) },
//       data: { extras: JSON.stringify(extras) },
//     });

//     res.status(200).json({ status: 'success', message: 'File deleted successfully' });
//   } catch (error) {
//     console.error('Error in deleteFile:', error);
//     res.status(500).json({ status: 'error', message: 'File delete failed' });
//   }
// };

// // List files for a specific plan
// export const listFiles = async (req, res) => {
//   try {
//     const { planId } = req.query;
//     if (!planId) {
//       return res.status(400).json({ status: 'error', message: 'Plan ID is required' });
//     }

//     const plan = await prisma.plan.findUnique({ where: { planId: parseInt(planId, 10) } });
//     if (!plan) {
//       return res.status(404).json({ status: 'error', message: 'Plan not found' });
//     }

//     const extras = plan.extras ? JSON.parse(plan.extras) : [];
//     res.status(200).json({ status: 'success', extras, message: 'Files retrieved successfully' });
//   } catch (error) {
//     console.error('Error in listFiles:', error);
//     res.status(500).json({ status: 'error', message: 'Error retrieving files' });
//   }
// };









// src/controllers/uploadController.js
import { validationResult } from 'express-validator';
import { uploadFileToS3, listFilesFromS3, deleteFileFromS3 } from '../services/uploadService.js';

// Upload a new file
export const uploadFile = async (req, res) => {
  try {
    uploadFileToS3(req, res, (err) => {
      if (err) {
        return res.status(500).json({ status: 'error', message: 'File upload failed', error: err.message });
      }
      if (!req.file) {
        return res.status(400).json({ status: 'error', message: 'No file provided' });
      }
      res.status(200).json({ status: 'success', fileUrl: req.file.location, message: 'File uploaded successfully' });
    });
  } catch (error) {
    console.error('Error in uploadFile:', error);
    res.status(500).json({ status: 'error', message: 'File upload failed' });
  }
};

// List files from S3
export const listFiles = async (req, res) => {
  try {
    const files = await listFilesFromS3();
    res.status(200).json({ status: 'success', files, message: 'Files retrieved successfully' });
  } catch (error) {
    console.error('Error in listFiles:', error);
    res.status(500).json({ status: 'error', message: 'Error retrieving files' });
  }
};

// Delete a file
export const deleteFile = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ status: 'error', errors: errors.array() });
  }

  try {
    const { fileKey } = req.body;

    if (!fileKey) {
      return res.status(400).json({ status: 'error', message: 'No file key provided' });
    }

    await deleteFileFromS3(fileKey);
    res.status(200).json({ status: 'success', message: 'File deleted successfully' });
  } catch (error) {
    console.error('Error in deleteFile:', error);
    res.status(500).json({ status: 'error', message: 'File delete failed' });
  }
};
