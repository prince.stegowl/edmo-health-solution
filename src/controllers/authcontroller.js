import {createOrUpdateUserOTP, verifyUserOTP} from '../services/authService.js'

export const loginUser = async (req, res) => {
  try {
    const { userNumber, userType } = req.body;
    if (!userType) {
      return res.status(400).json({ error: 'userType is required' });
    }
    const user = await createOrUpdateUserOTP(userType, userNumber);

    res.status(200).json({ userId: user.userId, otp: user.otp, isNewUser: !user.isVerified });
    // res.json({ userId: user[Object.keys(user).find(key => key.endsWith('Id'))], otp: user.otp, isNewUser: !user.isVerified });
  } catch (error) {
    console.error('Error logging in user:', error);
    res.status(500).json({ error: 'Error logging in user:' });
    
  }
};

export const verifyOTP = async (req, res) => {
  try {
    const { userNumber, otp, userType } = req.body;
    if (!userType) {
      return res.status(400).json({ error: 'userType is required' });
    }
    const result = await verifyUserOTP(userType, userNumber, otp);
    if (result.error) {
      return res.status(400).json({ error: result.error });
    }
    res.status(200).json({ message: result.message, token: result.token, userId: result.userId });
    // res.json({ message: result.message, token: result.token, userId: result.userId });
  } catch (error) {
    console.error('Error verifying OTP:', error);
    res.status(500).json({ error: 'Error verifying OTP:' });
  }
};





