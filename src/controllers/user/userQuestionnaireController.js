import userQuestionnaireService from '../../services/user/userQuestionnaireService.js';

const ApiResponse = (data = null, message = null, status = 200) => {
  return { data, message, status };
};

const createUserQuestionnaire = async (req, res) => {
  try {
    const newUserQuestionnaire = await userQuestionnaireService.createUserQuestionnaire(req.body);
    res.json(ApiResponse(newUserQuestionnaire, 'UserQuestionnaire created successfully', 201));
  } catch (error) {
    res.status(500).json(ApiResponse(null, error.message, 500));
  }
};

const getAllUserQuestionnaires = async (req, res) => {
  try {
    const userQuestionnaires = await userQuestionnaireService.getAllUserQuestionnaires();
    res.json(ApiResponse(userQuestionnaires, 'All UserQuestionnaires fetched successfully'));
  } catch (error) {
    res.status(500).json(ApiResponse(null, error.message, 500));
  }
};

const getUserQuestionnaireById = async (req, res) => {
  const { id } = req.params;
  try {
    const userQuestionnaire = await userQuestionnaireService.getUserQuestionnaireById(id);
    if (!userQuestionnaire) {
      return res.status(404).json(ApiResponse(null, 'UserQuestionnaire not found', 404));
    }
    res.json(ApiResponse(userQuestionnaire, 'UserQuestionnaire fetched successfully'));
  } catch (error) {
    res.status(500).json(ApiResponse(null, error.message, 500));
  }
};

const getUserQuestionnairesByUserId = async (req, res) => {
  const { id } = req.params;
  try {
    const userQuestionnaire = await userQuestionnaireService.getUserQuestionnairesByUserIdService(id);
    if (!userQuestionnaire) {
      return res.status(404).json(ApiResponse(null, 'UserQuestionnaire not found', 404));
    }
    res.json(ApiResponse(userQuestionnaire, 'UserQuestionnaire fetched successfully'));
  } catch (error) {
    res.status(500).json(ApiResponse(null, error.message, 500));
  }
};

const updateUserQuestionnaire = async (req, res) => {
  const { id } = req.params;
  try {
    const updatedUserQuestionnaire = await userQuestionnaireService.updateUserQuestionnaire(id, req.body);
    res.json(ApiResponse(updatedUserQuestionnaire, 'UserQuestionnaire updated successfully'));
  } catch (error) {
    res.status(500).json(ApiResponse(null, error.message, 500));
  }
};

const deleteUserQuestionnaire = async (req, res) => {
  const { id } = req.params;
  try {
    await userQuestionnaireService.deleteUserQuestionnaire(id);
    res.json(ApiResponse(null, 'UserQuestionnaire deleted successfully'));
  } catch (error) {
    res.status(500).json(ApiResponse(null, error.message, 500));
  }
};

export default {
  createUserQuestionnaire,
  getAllUserQuestionnaires,
  getUserQuestionnaireById,
  updateUserQuestionnaire,
  deleteUserQuestionnaire,
  getUserQuestionnairesByUserId
};
