import { getAllStaff, getStaffById, createStaff, updateStaff, deleteStaff } from '../../services/superAdmin/staffService.js';
import { validationResult } from 'express-validator';

// Standard API response function
const ApiResponse = (data = null, message = null, status = 200) => {
    return { data, message, status };
};

export const getAllStaffHandler = async (req, res) => {
    try {
        const staff = await getAllStaff();
        res.json(ApiResponse(staff, 'Staff fetched successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const getStaffByIdHandler = async (req, res) => {
    try {
        const staff = await getStaffById(req.params.id);
        if (!staff) {
            return res.status(404).json(ApiResponse(null, 'Staff not found', 404));
        }
        res.json(ApiResponse(staff, 'EHS Admin fetched successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const createStaffHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const newStaff = await createStaff(req.body);
        res.status(201).json(ApiResponse(newStaff, 'EHS Admin created successfully', 201));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const updateStaffHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const updatedStaff = await updateStaff(req.params.id, req.body);
        if (!updatedStaff) {
            return res.status(404).json(ApiResponse(null, 'Staff not found', 404));
        }
        res.json(ApiResponse(updatedStaff, 'EHS Admin updated successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const deleteStaffHandler = async (req, res) => {
    try {
        const deletedStaff = await deleteStaff(req.params.id);
        if (!deletedStaff) {
            return res.status(404).json(ApiResponse(null, 'Staff not found', 404));
        }
        res.json(ApiResponse(null, 'EHS Admin deleted successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};