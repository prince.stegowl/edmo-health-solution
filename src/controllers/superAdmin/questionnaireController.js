import { getAllQuestionnaires, getQuestionnaireById, createQuestionnaire, updateQuestionnaire, deleteQuestionnaire } from '../../services/superAdmin/questionnaireService.js';
import { validationResult } from 'express-validator';

// Standard API response function
const ApiResponse = (data = null, message = null, status = 200) => {
    return { data, message, status };
};

export const getAllQuestionnairesHandler = async (req, res) => {
    try {
        const questionnaires = await getAllQuestionnaires();
        res.json(ApiResponse(questionnaires, 'Questionnaires fetched successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const getQuestionnaireByIdHandler = async (req, res) => {
    try {
        const questionnaire = await getQuestionnaireById(req.params.id);
        if (!questionnaire) {
            return res.status(404).json(ApiResponse(null, 'Questionnaire not found', 404));
        }
        res.json(ApiResponse(questionnaire, 'Questionnaire fetched successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const createQuestionnaireHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const newQuestionnaire = await createQuestionnaire(req.body);
        res.status(201).json(ApiResponse(newQuestionnaire, 'Questionnaire created successfully', 201));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const updateQuestionnaireHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const updatedQuestionnaire = await updateQuestionnaire(req.params.id, req.body);
        if (!updatedQuestionnaire) {
            return res.status(404).json(ApiResponse(null, 'Questionnaire not found', 404));
        }
        res.json(ApiResponse(updatedQuestionnaire, 'Questionnaire updated successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const deleteQuestionnaireHandler = async (req, res) => {
    try {
        const deletedQuestionnaire = await deleteQuestionnaire(req.params.id);
        if (!deletedQuestionnaire) {
            return res.status(404).json(ApiResponse(null, 'Questionnaire not found', 404));
        }
        res.json(ApiResponse(null, 'Questionnaire deleted successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};