// import { getAllHealthExperts, getHealthExpertById, createHealthExpert, updateHealthExpert, deleteHealthExpert } from '../../services/superAdmin/healthExpertService.js  ';
// import { validationResult } from 'express-validator';

// // Standard API response function
// const ApiResponse = (data = null, message = null, status = 200) => {
//     return { data, message, status };
// };

// export const getAllHealthExpertsHandler = async (req, res) => {
//     try {
//         const healthExperts = await getAllHealthExperts();
//         res.json(ApiResponse(healthExperts, 'Health Experts fetched successfully', 200));
//     } catch (error) {
//         res.status(500).json(ApiResponse(null, error.message, 500));
//     }
// };

// export const getHealthExpertByIdHandler = async (req, res) => {
//     try {
//         const healthExpert = await getHealthExpertById(req.params.id);
//         if (!healthExpert) {
//             return res.status(404).json(ApiResponse(null, 'Health Expert not found', 404));
//         }
//         res.json(ApiResponse(healthExpert, 'Health Expert fetched successfully', 200));
//     } catch (error) {
//         res.status(500).json(ApiResponse(null, error.message, 500));
//     }
// };

// export const createHealthExpertHandler = async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         return res.status(400).json(ApiResponse(null, errors.array(), 400));
//     }

//     try {
//         const newHealthExpert = await createHealthExpert(req.body);
//         res.status(201).json(ApiResponse(newHealthExpert, 'Health Expert created successfully', 201));
//     } catch (error) {
//         res.status(500).json(ApiResponse(null, error.message, 500));
//     }
// };

// export const updateHealthExpertHandler = async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         return res.status(400).json(ApiResponse(null, errors.array(), 400));
//     }

//     try {
//         const updatedHealthExpert = await updateHealthExpert(req.params.id, req.body);
//         if (!updatedHealthExpert) {
//             return res.status(404).json(ApiResponse(null, 'Health Expert not found', 404));
//         }
//         res.json(ApiResponse(updatedHealthExpert, 'Health Expert updated successfully', 200));
//     } catch (error) {
//         res.status(500).json(ApiResponse(null, error.message, 500));
//     }
// };

// export const deleteHealthExpertHandler = async (req, res) => {
//     try {
//         const deletedHealthExpert = await deleteHealthExpert(req.params.id);
//         if (!deletedHealthExpert) {
//             return res.status(404).json(ApiResponse(null, 'Health Expert not found', 404));
//         }
//         res.json(ApiResponse(null, 'Health Expert deleted successfully', 200));
//     } catch (error) {
//         res.status(500).json(ApiResponse(null, error.message, 500));
//     }
// };



import { getAllHealthExperts, getHealthExpertById, createHealthExpert, updateHealthExpert, deleteHealthExpert } from '../../services/superAdmin/healthExpertService.js';
import { validationResult } from 'express-validator';

// Standard API response function
const ApiResponse = (data = null, message = null, status = 200) => {
    return { data, message, status };
};

export const getAllHealthExpertsHandler = async (req, res) => {
    try {
        const healthExperts = await getAllHealthExperts();
        res.json(ApiResponse(healthExperts, 'Health Experts fetched successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const getHealthExpertByIdHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const healthExpert = await getHealthExpertById(req.params.id);
        if (!healthExpert) {
            return res.status(404).json(ApiResponse(null, 'Health Expert not found', 404));
        }
        res.json(ApiResponse(healthExpert, 'Health Expert fetched successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const createHealthExpertHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const newHealthExpert = await createHealthExpert(req.body);
        res.status(201).json(ApiResponse(newHealthExpert, 'Health Expert created successfully', 201));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const updateHealthExpertHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const updatedHealthExpert = await updateHealthExpert(req.params.id, req.body);
        if (!updatedHealthExpert) {
            return res.status(404).json(ApiResponse(null, 'Health Expert not found', 404));
        }
        res.json(ApiResponse(updatedHealthExpert, 'Health Expert updated successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const deleteHealthExpertHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const deletedHealthExpert = await deleteHealthExpert(req.params.id);
        if (!deletedHealthExpert) {
            return res.status(404).json(ApiResponse(null, 'Health Expert not found', 404));
        }
        res.json(ApiResponse(null, 'Health Expert deleted successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};
