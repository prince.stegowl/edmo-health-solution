// // src/controllers/plansController.js
// import { getAllPlans, createPlan, updatePlan, deletePlan, getPlanById } from '../services/plansService.js';

// export const fetchAllPlans = async (req, res) => {
//   try {
//     const plans = await getAllPlans();
//     res.status(200).json({ data: plans, status: 'success', message: null });
//   } catch (error) {
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };

// export const fetchPlanById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const plan = await getPlanById(id);
//     if (!plan) {
//       return res.status(404).json({ data: null, status: 'error', message: 'Plan not found' });
//     }
//     res.status(200).json({ data: plan, status: 'success', message: null });
//   } catch (error) {
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };

// // export const addNewPlan = async (req, res) => {
// //   try {
// //     const { planName, planPrice, planDuration, planDescription, healthExpertAppointments, doctorAppointments, extras } = req.body;
// //     const image = req.body.image;

// //     const planData = {
// //       planName,
// //       planPrice: parseInt(planPrice, 10),
// //       planDuration,
// //       planDescription,
// //       healthExpertAppointments: parseInt(healthExpertAppointments, 10),
// //       doctorAppointments: parseInt(doctorAppointments, 10),
// //       image,
// //       extras: JSON.stringify(extras), // Store extras as a JSON string
// //     };

// //     const newPlan = await createPlan(planData);
// //     res.status(201).json({ data: newPlan, status: 'success', message: 'Plan created successfully' });
// //   } catch (error) {
// //     console.error('Error in addNewPlan:', error);
// //     res.status(500).json({ data: null, status: 'error', message: error.message });
// //   }
// // };

// export const addNewPlan = async (req, res) => {
//   try {
//     const {
//       planName,
//       planPrice,
//       planDuration,
//       planDescription,
//       healthExpertAppointments,
//       doctorAppointments,
//       extras,
//     } = req.body;
//     console.log(req.body, " reqesttttt")

//     console.log(typeof extras,"......................extrassss")

//     const image = req.files && req.files['image'] ? req.files['image'][0].location : null;

//     // Log received data for debugging
//    // console.log('Received Data:', { planName, planPrice, planDuration, planDescription, healthExpertAppointments, doctorAppointments, image, extras });

//     const planData = {
//       planName,
//       planPrice: parseInt(planPrice, 10),
//       planDuration,
//       planDescription,
//       healthExpertAppointments: parseInt(healthExpertAppointments, 10),
//       doctorAppointments: parseInt(doctorAppointments, 10),
//       image,
//       extras, // Directly store extras as it is already a JSON string
//     };

//     const newPlan = await createPlan(planData);
//     res.status(201).json({ data: newPlan, status: 'success', message: 'Plan created successfully' });
//   } catch (error) {
//     console.error('Error in addNewPlan:', error);
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };



// export const modifyPlan = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { planName, planPrice, planDuration, planDescription, healthExpertAppointments, doctorAppointments, extras } = req.body;
//     const image = req.body.image;

//     const planData = {
//       planName,
//       planPrice: parseInt(planPrice, 10),
//       planDuration,
//       planDescription,
//       healthExpertAppointments: parseInt(healthExpertAppointments, 10),
//       doctorAppointments: parseInt(doctorAppointments, 10),
//       image,
//       extras: JSON.stringify(extras), // Store extras as a JSON string
//     };

//     const updatedPlan = await updatePlan(id, planData);
//     res.status(200).json({ data: updatedPlan, status: 'success', message: 'Plan updated successfully' });
//   } catch (error) {
//     console.error('Error in modifyPlan:', error);
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };

// export const removePlan = async (req, res) => {
//   try {
//     const { id } = req.params;
//     await deletePlan(id);
//     res.status(200).json({ data: null, status: 'success', message: 'Plan deleted successfully' });
//   } catch (error) {
//     console.error('Error in removePlan:', error);
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };









// import { validationResult } from 'express-validator';
// import { createPlan, getPlans, getPlanById, updatePlanById, deletePlanById } from '../services/plansService.js';

// export const addNewPlan = async (req, res) => {
//   try {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ status: 'error', errors: errors.array() });
//     }

//     const {
//       planName,
//       planPrice,
//       planDuration,
//       planDescription,
//       healthExpertAppointments,
//       doctorAppointments,
//       extras,
//     } = req.body;

//     const image = req.files && req.files['image'] ? req.files['image'][0].location : null;

//     // Log received data for debugging
//     console.log('Received Data:', { planName, planPrice, planDuration, planDescription, healthExpertAppointments, doctorAppointments, image, extras });

//     const planData = {
//       planName,
//       planPrice: parseInt(planPrice, 10),
//       planDuration,
//       planDescription,
//       healthExpertAppointments: parseInt(healthExpertAppointments, 10),
//       doctorAppointments: parseInt(doctorAppointments, 10),
//       image,
//       extras, // Directly store extras as it is already a JSON string
//     };

//     const newPlan = await createPlan(planData);
//     res.status(201).json({ data: newPlan, status: 'success', message: 'Plan created successfully' });
//   } catch (error) {
//     console.error('Error in addNewPlan:', error);
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };

// export const getAllPlans = async (req, res) => {
//   try {
//     const plans = await getPlans();
//     res.status(200).json({ data: plans, status: 'success', message: 'Plans retrieved successfully' });
//   } catch (error) {
//     console.error('Error in getAllPlans:', error);
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };

// export const getSinglePlanById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const plan = await getPlanById(id);
//     if (!plan) {
//       return res.status(404).json({ data: null, status: 'error', message: 'Plan not found' });
//     }
//     res.status(200).json({ data: plan, status: 'success', message: 'Plan retrieved successfully' });
//   } catch (error) {
//     console.error('Error in getSinglePlanById:', error);
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };

// export const updatePlan = async (req, res) => {
//   try {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).json({ status: 'error', errors: errors.array() });
//     }

//     const { id } = req.params;
//     const {
//       planName,
//       planPrice,
//       planDuration,
//       planDescription,
//       healthExpertAppointments,
//       doctorAppointments,
//       extras,
//     } = req.body;

//     const image = req.files && req.files['image'] ? req.files['image'][0].location : null;

//     const planData = {
//       planName,
//       planPrice: parseInt(planPrice, 10),
//       planDuration,
//       planDescription,
//       healthExpertAppointments: parseInt(healthExpertAppointments, 10),
//       doctorAppointments: parseInt(doctorAppointments, 10),
//       image,
//       extras, // Directly store extras as it is already a JSON string
//     };

//     const updatedPlan = await updatePlanById(id, planData);
//     if (!updatedPlan) {
//       return res.status(404).json({ data: null, status: 'error', message: 'Plan not found' });
//     }
//     res.status(200).json({ data: updatedPlan, status: 'success', message: 'Plan updated successfully' });
//   } catch (error) {
//     console.error('Error in updatePlan:', error);
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };

// // export const disablePlan = async (req, res) => {
// //   try {
// //     const { id } = req.params;
// //     const disabledPlan = await disablePlanById(id);
// //     if (!disabledPlan) {
// //       return res.status(404).json({ data: null, status: 'error', message: 'Plan not found' });
// //     }
// //     res.status(200).json({ data: disabledPlan, status: 'success', message: 'Plan disabled successfully' });
// //   } catch (error) {
// //     console.error('Error in disablePlan:', error);
// //     res.status(500).json({ data: null, status: 'error', message: error.message });
// //   }
// // };


// export const deletePlan = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const deletedPlan = await deletePlanById(id);
//     if (!deletedPlan) {
//       return res.status(404).json({ data: null, status: 'error', message: 'Plan not found' });
//     }
//     res.status(200).json({ data: deletedPlan, status: 'success', message: 'Plan deleted successfully' });
//   } catch (error) {
//     console.error('Error in deletePlan:', error);
//     res.status(500).json({ data: null, status: 'error', message: error.message });
//   }
// };







import { validationResult } from 'express-validator';
import { createPlan, getPlans, getPlanById, updatePlanById, updatePlanStatusById } from '../../services/superAdmin/plansService.js';

export const addNewPlan = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 'error', errors: errors.array() });
    }

    const {
      planName,
      planPrice,
      planDuration,
      planDescription,
      healthExpertAppointments,
      doctorAppointments,
      extras,
      image, // Assume image is sent as a string
    } = req.body;

    // Log received data for debugging
    console.log('Received Data:', { planName, planPrice, planDuration, planDescription, healthExpertAppointments, doctorAppointments, image, extras });

    const planData = {
      planName,
      planPrice: parseInt(planPrice, 10),
      planDuration,
      planDescription,
      healthExpertAppointments: parseInt(healthExpertAppointments, 10),
      doctorAppointments: parseInt(doctorAppointments, 10),
      image,
      extras, // Directly store extras as it is already a JSON string
    };

    const newPlan = await createPlan(planData);
    res.status(201).json({ data: newPlan, status: 'success', message: 'Plan created successfully' });
  } catch (error) {
    console.error('Error in addNewPlan:', error);
    res.status(500).json({ data: null, status: 'error', message: error.message });
  }
};

export const updatePlan = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 'error', errors: errors.array() });
    }

    const { id } = req.params;
    const {
      planName,
      planPrice,
      planDuration,
      planDescription,
      healthExpertAppointments,
      doctorAppointments,
      extras,
      image, // Assume image is sent as a string
    } = req.body;

    const planData = {
      planName,
      planPrice: parseInt(planPrice, 10),
      planDuration,
      planDescription,
      healthExpertAppointments: parseInt(healthExpertAppointments, 10),
      doctorAppointments: parseInt(doctorAppointments, 10),
      image,
      extras, // Directly store extras as it is already a JSON string
    };

    const updatedPlan = await updatePlanById(id, planData);
    if (!updatedPlan) {
      return res.status(404).json({ data: null, status: 'error', message: 'Plan not found' });
    }
    res.status(200).json({ data: updatedPlan, status: 'success', message: 'Plan updated successfully' });
  } catch (error) {
    console.error('Error in updatePlan:', error);
    res.status(500).json({ data: null, status: 'error', message: error.message });
  }
};

export const getAllPlans = async (req, res) => {
  try {
    const plans = await getPlans();
    res.status(200).json({ data: plans, status: 'success', message: 'Plans retrieved successfully' });
  } catch (error) {
    console.error('Error in getAllPlans:', error);
    res.status(500).json({ data: null, status: 'error', message: error.message });
  }
};

export const getSinglePlanById = async (req, res) => {
  try {
    const { id } = req.params;
    const plan = await getPlanById(id);
    if (!plan) {
      return res.status(404).json({ data: null, status: 'error', message: 'Plan not found' });
    }
    res.status(200).json({ data: plan, status: 'success', message: 'Plan retrieved successfully' });
  } catch (error) {
    console.error('Error in getSinglePlanById:', error);
    res.status(500).json({ data: null, status: 'error', message: error.message });
  }
};
export const updatePlanStatus = async (req, res) => {
  try {
    const { id } = req.params;

    const toggledPlan = await updatePlanStatusById(id);
    if (!toggledPlan) {
      return res.status(404).json({ data: null, status: 'error', message: 'Plan not found' });
    }
    res.status(200).json({ data: toggledPlan, status: 'success', message: 'Plan status toggled successfully' });
  } catch (error) {
    console.error('Error in updatePlanStatus:', error);
    res.status(500).json({ data: null, status: 'error', message: error.message });
  }
};
