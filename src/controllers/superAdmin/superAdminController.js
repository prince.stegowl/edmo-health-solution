// // const prisma = require('../prisma');
// import prisma from '../prisma.js';

// // Get SuperAdmin by ID

// export const getSuperAdminById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     console.log("this is Id .........",id)
//     const superAdmin = await prisma.superAdmin.findUnique({
//       where: { superAdminId: Number(id) }, // Ensure the ID is an integer
//     });
   
//     if (!superAdmin) {
//       return res.status(404).json({ error: 'SuperAdmin not found' });
//     }

//     res.json(superAdmin);
//   } catch (error) {
//     console.error('Error fetching SuperAdmin:', error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// };

// // Update SuperAdmin by ID
// export const updateSuperAdminById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { superAdminName, superAdminEmail } = req.body;

//     const updatedSuperAdmin = await prisma.superAdmin.update({
//       where: { superAdminId: parseInt(id, 10) }, // Ensure the ID is an integer
//       data: {
//         superAdminName,
//         superAdminEmail,
//       },
//     });

//     res.json(updatedSuperAdmin);
//   } catch (error) {
//     console.error('Error updating SuperAdmin:', error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// };

// // module.exports = {
// //   getSuperAdminById,
// //   updateSuperAdminById,
// // };






// import prisma from '../prisma.js';

// export const updateSuperAdminById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { superAdminName, superAdminEmail, userNumber } = req.body;
//     const image = req.file ? req.file.location : null;

//     const updatedData = {
//       superAdminName,
//       superAdminEmail,
//       userNumber,
//     };

//     if (image) {
//       updatedData.image = image; // Add the image URL to the update data
//     }

//     const updatedSuperAdmin = await prisma.superAdmin.update({
//       where: { superAdminId: parseInt(id, 10) },
//       data: updatedData,
//     });

//     res.status(200).json(updatedSuperAdmin);
//   } catch (error) {
//     console.error('Error updating SuperAdmin:', error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// };

// export const getSuperAdminById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const superAdmin = await prisma.superAdmin.findUnique({
//       where: { superAdminId: parseInt(id, 10) },
//     });

//     if (!superAdmin) {
//       return res.status(404).json({ error: 'SuperAdmin not found' });
//     }

//     res.status(200).json(superAdmin);
//   } catch (error) {
//     console.error('Error fetching SuperAdmin:', error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// };







// import prisma from '../prisma.js';

// const ApiResponse = (data = null, error = null, status = 200) => {
//   return { data: data, error: error, status: status };
// };

// export const updateSuperAdminById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { superAdminName, superAdminEmail, userNumber } = req.body;
//     const image = req.file ? req.file.location : null;

//     const updatedData = {
//       superAdminName,
//       superAdminEmail,
//       userNumber,
//     };

//     if (image) {
//       updatedData.image = image; // Add the image URL to the update data
//     }

//     const updatedSuperAdmin = await prisma.superAdmin.update({
//       where: { superAdminId: parseInt(id, 10) },
//       data: updatedData,
//     });

//     const response = ApiResponse(updatedSuperAdmin);
//     res.status(200).json(response);
//   } catch (error) {
//     console.error('Error updating SuperAdmin:', error);
//     const response = ApiResponse(null, 'Internal Server Error', 500);
//     res.status(500).json(response);
//   }
// };

// export const getSuperAdminById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const superAdmin = await prisma.superAdmin.findUnique({
//       where: { superAdminId: parseInt(id, 10) },
//     });

//     if (!superAdmin) {
//       const response = ApiResponse(null, 'SuperAdmin not found', 404);
//       return res.status(404).json(response);
//     }

//     const response = ApiResponse(superAdmin);
//     res.status(200).json(response);
//   } catch (error) {
//     console.error('Error fetching SuperAdmin:', error);
//     const response = ApiResponse(null, 'Internal Server Error', 500);
//     res.status(500).json(response);
//   }
// };



import prisma from '../../prisma.js';

const ApiResponse = (data = null, message = null, status = 200) => {
  return { data: data, message: message, status: status };
};

export const updateSuperAdminById = async (req, res) => {
  try {
    const { id } = req.params;
    const { superAdminName, superAdminEmail, userNumber, image } = req.body;

    const updatedData = {
      superAdminName,
      superAdminEmail,
      userNumber,
    };

    // Check if image URL is provided and update the data accordingly
    if (image) {
      updatedData.image = image;
    }

    const updatedSuperAdmin = await prisma.superAdmin.update({
      where: { superAdminId: parseInt(id, 10) },
      data: updatedData,
    });

    const response = ApiResponse(updatedSuperAdmin);
    res.status(200).json(response);
  } catch (error) {
    console.error('Error updating SuperAdmin:', error);
    const response = ApiResponse(null, 'Internal Server Error', 500);
    res.status(500).json(response);
  }
};


// export const updateSuperAdminById = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { superAdminName, superAdminEmail, userNumber } = req.body;
//     const image = req.file ? req.file.location : null;

//     const updatedData = {
//       superAdminName,
//       superAdminEmail,
//       userNumber,
//     };

//     if (image) {
//       updatedData.image = image; // Add the image URL to the update data
//     }

//     const updatedSuperAdmin = await prisma.superAdmin.update({
//       // where: { superAdminId: parseInt(id) },
//       where: { superAdminId: parseInt(id, 10) },
//       data: updatedData,
//     });

//     const response = ApiResponse(updatedSuperAdmin);
//     res.status(200).json(response);
//   } catch (error) {
//     console.error('Error updating SuperAdmin:', error);
//     const response = ApiResponse(null, 'Internal Server Error', 500);
//     res.status(500).json(response);
//   }
// };

export const getSuperAdminById = async (req, res) => {
  try {
    const { id } = req.params;
    const superAdmin = await prisma.superAdmin.findUnique({
      where: { superAdminId: parseInt(id, 10) },
    });

    if (!superAdmin) {
      const response = ApiResponse(null, 'SuperAdmin not found', 404);
      return res.status(404).json(response);
    }

    const response = ApiResponse(superAdmin);
    res.status(200).json(response);
  } catch (error) {
    console.error('Error fetching SuperAdmin:', error);
    const response = ApiResponse(null, 'Internal Server Error', 500);
    res.status(500).json(response);
  }
};


