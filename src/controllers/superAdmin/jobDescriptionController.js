import { validationResult } from 'express-validator';
import { getAllJobDescriptionsService, getJobDescriptionByIdService, createJobDescriptionService, updateJobDescriptionService , deleteJobDescriptionService} from '../../services/superAdmin/jobDescriptionServices.js';

const ApiResponse = (data = null, message = null, status = 200) => {
    return { data, message, status };
};

export const getAllJobDescriptions = async (req, res) => {
    try {
        const jobDescriptions = await getAllJobDescriptionsService();
        res.status(200).json(ApiResponse(jobDescriptions, 'Job descriptions fetched successfully'));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const getJobDescriptionById = async (req, res) => {
    try {
        const jobDescription = await getJobDescriptionByIdService(req.params.id);
        if (!jobDescription) {
            return res.status(404).json(ApiResponse(null, 'Job description not found', 404));
        }
        res.status(200).json(ApiResponse(jobDescription, 'Job description fetched successfully'));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const createJobDescription = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const createdJobDescription = await createJobDescriptionService(req.body);
        res.status(201).json(ApiResponse(createdJobDescription, 'Job description created successfully', 201));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const updateJobDescription = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const updatedJobDescription = await updateJobDescriptionService(req.params.id, req.body);
        if (!updatedJobDescription) {
            return res.status(404).json(ApiResponse(null, 'Job description not found', 404));
        }
        res.status(200).json(ApiResponse(updatedJobDescription, 'Job description updated successfully'));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const deleteJobDescription = async (req, res) => {
    try {
        const deletedJobDescription = await deleteJobDescriptionService(req.params.id);
        if (!deletedJobDescription) {
            return res.status(404).json(ApiResponse(null, 'Job description not found', 404));
        }
        res.status(200).json(ApiResponse(null, 'Job description deleted successfully'));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};
