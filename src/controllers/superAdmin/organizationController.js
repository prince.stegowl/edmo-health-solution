import { getAllOrganizations, getOrganizationById, createOrganization, updateOrganization, deleteOrganization } from '../../services/superAdmin/organizationService.js';
import { validationResult } from 'express-validator';

// Standard API response function
const ApiResponse = (data = null, message = null, status = 200) => {
    return { data, message, status };
};

export const getAllOrganizationsHandler = async (req, res) => {
    try {
        const organizations = await getAllOrganizations();
        res.json(ApiResponse(organizations, 'Organizations fetched successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const getOrganizationByIdHandler = async (req, res) => {
    try {
        const organization = await getOrganizationById(req.params.id);
        if (!organization) {
            return res.status(404).json(ApiResponse(null, 'Organization not found', 404));
        }
        res.json(ApiResponse(organization, 'Organization fetched successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const createOrganizationHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const newOrganization = await createOrganization(req.body);
        res.status(201).json(ApiResponse(newOrganization, 'Organization created successfully', 201));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const updateOrganizationHandler = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const updatedOrganization = await updateOrganization(req.params.id, req.body);
        if (!updatedOrganization) {
            return res.status(404).json(ApiResponse(null, 'Organization not found', 404));
        }
        res.json(ApiResponse(updatedOrganization, 'Organization updated successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const deleteOrganizationHandler = async (req, res) => {
    try {
        const deletedOrganization = await deleteOrganization(req.params.id);
        if (!deletedOrganization) {
            return res.status(404).json(ApiResponse(null, 'Organization not found', 404));
        }
        res.json(ApiResponse(null, 'Organization deleted successfully', 200));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};
