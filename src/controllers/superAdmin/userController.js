import { validationResult } from 'express-validator';
import { getAllUsersService, getUserByIdService, createUserService, updateUserService, deleteUserService } from '../../services/superAdmin/userService.js';

const ApiResponse = (data = null, message = null, status = 200) => {
    return { data, message, status };
};

// export const getAllUsers = async (req, res) => {
//     try {
//         const users = await getAllUsersService();
//         res.status(200).json(ApiResponse(users, 'Users fetched successfully'));

//     } catch (error) {
//         res.status(500).json(ApiResponse(null, error.message, 500));
//     }
// };
export const getAllUsers = async (req, res) => {
    try {
        // Extract pagination parameters from query
        const page = parseInt(req.query.page, 10) || 1;
        const pageSize = parseInt(req.query.pageSize, 10) || 10;

        const result = await getAllUsersService(page, pageSize);

        res.status(200).json(ApiResponse(result, 'Users fetched successfully'));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const getUserById = async (req, res) => {

    try {
        const user = await getUserByIdService(req.params.id);
        if (!user) {
            return res.status(404).json(ApiResponse(null, 'User not found', 404));
        }
        res.status(200).json(ApiResponse(user, 'User fetched successfully'));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

export const createUser = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const createdUser = await createUserService(req.body);
        res.status(201).json(ApiResponse(createdUser, 'User created successfully', 201));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};


export const updateUser = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const updatedUser = await updateUserService(req.params.id, req.body);
        if (!updatedUser) {
            return res.status(404).json(ApiResponse(null, 'User not found', 404));
        }
        res.status(200).json(ApiResponse(updatedUser, 'User updated successfully'));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};

// export const updateUser = async (req, res) => {
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         return res.status(400).json(ApiResponse(null, errors.array(), 400));
//     }

//     try {
//         const updatedUser = await updateUserService(req.params.id, req.body);
//         if (!updatedUser) {
//             return res.status(404).json(ApiResponse(null, 'User not found', 404));
//         }
//         res.status(200).json(ApiResponse(updatedUser, 'User updated successfully'));
//     } catch (error) {
//         res.status(500).json(ApiResponse(null, error.message, 500));
//     }
// };

export const deleteUser = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json(ApiResponse(null, errors.array(), 400));
    }

    try {
        const deletedUser = await deleteUserService(req.params.id);
        if (!deletedUser) {
            return res.status(404).json(ApiResponse(null, 'User not found', 404));
        }
        res.status(200).json(ApiResponse(null, 'User deleted successfully'));
    } catch (error) {
        res.status(500).json(ApiResponse(null, error.message, 500));
    }
};
