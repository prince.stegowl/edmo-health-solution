import { getUsersByOrganizationId } from '../../services/organization/organizationUsersService.js';

const ApiResponse = (data = null, message = null, status = 200) => {
    return { data, message, status };
};

export const getUsersByOrganizationIdController = async (req, res) => {
  const { organizationId } = req.params;

  try {
    const users = await getUsersByOrganizationId(organizationId);
    res.status(200).json(ApiResponse(users, 'Users fetched successfully.'));
  } catch (error) {
    console.error(error);
    res.status(500).json(ApiResponse(null, 'An error occurred while fetching users.', 500));
  }
};