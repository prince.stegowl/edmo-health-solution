// // src/services/uploadService.js
// import s3 from '../config/awsConfig.js';

// export const uploadFileToS3 = (file) => {
//   const params = {
//     Bucket: process.env.AWS_S3_BUCKET_NAME,
//     Key: Date.now().toString() + '-' + file.originalname,
//     Body: file.buffer,
//     ContentType: file.mimetype,
//   };

//   return new Promise((resolve, reject) => {
//     s3.upload(params, (err, data) => {
//       if (err) {
//         return reject(err);
//       }
//       resolve(data.Location);
//     });
//   });
// };

// export const deleteFileFromS3 = (fileKey) => {
//   const params = {
//     Bucket: process.env.AWS_S3_BUCKET_NAME,
//     Key: fileKey,
//   };

//   return new Promise((resolve, reject) => {
//     s3.deleteObject(params, (err, data) => {
//       if (err) {
//         return reject(err);
//       }
//       resolve(data);
//     });
//   });
// };

// export const listFilesFromS3 = () => {
//   const params = {
//     Bucket: process.env.AWS_S3_BUCKET_NAME,
//   };

//   return new Promise((resolve, reject) => {
//     s3.listObjectsV2(params, (err, data) => {
//       if (err) {
//         return reject(err);
//       }
//       resolve(data.Contents);
//     });
//   });
// };



// src/services/uploadService.js
import multer from 'multer';
import multerS3 from 'multer-s3';
import s3 from '../config/awsConfig.js';

const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.AWS_S3_BUCKET_NAME,
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString() + '-' + file.originalname);
    },
  }),
});

export const uploadFileToS3 = upload.single('image');

export const listFilesFromS3 = async () => {
  const params = {
    Bucket: process.env.AWS_S3_BUCKET_NAME,
  };

  try {
    const data = await s3.listObjectsV2(params).promise();
    return data.Contents.map((item) => ({
      key: item.Key,
      url: `https://${params.Bucket}.s3.amazonaws.com/${item.Key}`,
    }));
  } catch (error) {
    throw new Error('Error listing files from S3');
  }
};

export const deleteFileFromS3 = async (key) => {
  const params = {
    Bucket: process.env.AWS_S3_BUCKET_NAME,
    Key: key,
  };

  try {
    await s3.deleteObject(params).promise();
  } catch (error) {
    throw new Error('Error deleting file from S3');
  }
};
