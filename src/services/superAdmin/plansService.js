// // src/services/plansService.js
// import prisma from '../prisma.js';

// export const getAllPlans = async () => {
//   try {
//     const plans = await prisma.plan.findMany();
//     return plans;
//   } catch (error) {
//     throw new Error('Error fetching plans');
//   }
// };

// export const getPlanById = async (planId) => {
//   try {
//     const plan = await prisma.plan.findUnique({
//       where: { planId: parseInt(planId, 10) },
//     });
//     return plan;
//   } catch (error) {
//     throw new Error('Error fetching plan');
//   }
// };

// export const createPlan = async (planData) => {
//   try {
//     const newPlan = await prisma.plan.create({
//       data: planData,
//     });
//     return newPlan;
//   } catch (error) {
//     throw new Error('Error creating plan');
//   }
// };

// export const updatePlan = async (planId, planData) => {
//   try {
//     const updatedPlan = await prisma.plan.update({
//       where: { planId: parseInt(planId, 10) },
//       data: planData,
//     });
//     return updatedPlan;
//   } catch (error) {
//     throw new Error('Error updating plan');
//   }
// };

// export const deletePlan = async (planId) => {
//   try {
//     await prisma.plan.delete({
//       where: { planId: parseInt(planId, 10) },
//     });
//   } catch (error) {
//     throw new Error('Error deleting plan');
//   }
// };




import prisma from '../../prisma.js';

export const createPlan = async (planData) => {
  try {
    const newPlan = await prisma.plan.create({
      data: planData,
    });
    return newPlan;
  } catch (error) {
    console.error('Error in createPlan:', error);
    throw new Error('Error creating plan');
  }
};

export const getPlans = async () => {
  try {
    const plans = await prisma.plan.findMany();
    return plans;
  } catch (error) {
    console.error('Error in getPlans:', error);
    throw new Error('Error retrieving plans');
  }
};

export const getPlanById = async (id) => {
  try {
    const plan = await prisma.plan.findUnique({
      where: { planId: parseInt(id, 10) },
    });
    return plan;
  } catch (error) {
    console.error('Error in getPlanById:', error);
    throw new Error('Error retrieving plan');
  }
};

export const updatePlanById = async (id, planData) => {
  try {
    const updatedPlan = await prisma.plan.update({
      where: { planId: parseInt(id, 10) },
      data: planData,
    });
    return updatedPlan;
  } catch (error) {
    console.error('Error in updatePlanById:', error);
    throw new Error('Error updating plan');
  }
};

// export const deletePlanById = async (id) => {
//   try {
//     const deletedPlan = await prisma.plan.delete({
//       where: { planId: parseInt(id, 10) },
//     });
//     return deletedPlan;
//   } catch (error) {
//     console.error('Error in deletePlanById:', error);
//     throw new Error('Error deleting plan');
//   }
// };
export const updatePlanStatusById = async (id) => {
  try {
    // Fetch the current status of the plan
    const plan = await prisma.plan.findUnique({
      where: { planId: parseInt(id, 10) },
    });

    if (!plan) {
      return null;
    }

    // Toggle the active status
    const updatedPlan = await prisma.plan.update({
      where: { planId: parseInt(id, 10) },
      data: { active: !plan.active },
    });
    return updatedPlan;
  } catch (error) {
    console.error('Error in togglePlanStatusById:', error);
    throw new Error('Error toggling plan status');
  }
};

