// import prisma from '../../prisma.js';

// export const getAllStaff = async () => {
//     return await prisma.staff.findMany();
// };

// export const getStaffById = async (id) => {
//     return await prisma.staff.findUnique({ where: { staffId: Number(id) } });
// };

// export const createStaff = async (staffData) => {
//     return await prisma.staff.create({
//         data: {
//             ...staffData,
//             createdAt: new Date(),
//         }
//     });
// };

// export const updateStaff = async (id, staffData) => {
//     return await prisma.staff.update({
//         where: { staffId: Number(id) },
//         data: {
//             ...staffData,
//             updatedAt: new Date(),
//         },
//     });
// };

// export const deleteStaff = async (id) => {
//     return await prisma.staff.delete({ where: { staffId: Number(id) } });
// };

import prisma from '../../prisma.js';


export const getAllStaff = async () => {
    return await prisma.staff.findMany({
        include: {
            staffJobDescriptions: {
                include: {
                    jobDescription: true
                }
            }
        }
    });
};

export const getStaffById = async (id) => {
    return await prisma.staff.findUnique({
        where: { staffId: parseInt(id, 10) },
        include: {
            staffJobDescriptions: {
                include: {
                    jobDescription: true
                }
            }
        }
    });
};


export const createStaff = async (data) => {
    const { jobDescriptionId, ...staffData } = data;

    // Check if the jobDescriptionId exists in the JobDescription table
    const existingJobDescription = await prisma.jobDescription.findUnique({
        where: {
            jobDescriptionId: parseInt(jobDescriptionId, 10),
        },
    });

    if (!existingJobDescription) {
        throw new Error(`JobDescription with id ${jobDescriptionId} does not exist.`);
    }

    // Create staff
    const createdStaff = await prisma.staff.create({
        data: {
            ...staffData,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
    });

    // Create entry in the StaffJobDescription table
    if (jobDescriptionId) {
        await prisma.staffJobDescription.create({
            data: {
                staffId: createdStaff.staffId,
                jobDescriptionId: parseInt(jobDescriptionId, 10),
            },
        });
    }

    return createdStaff;
};

// export const updateStaff = async (id, data) => {
//     const { jobDescriptionId, ...staffData } = data;

//     // Update staff data
//     const updatedStaff = await prisma.staff.update({
//         where: { staffId: parseInt(id, 10) },
//         data: {
//             ...staffData,
//             updatedAt: new Date(),
//         },
//     });

//     // Update the StaffJobDescription relationship
//     const staffJobDesc = await prisma.staffJobDescription.findUnique({
//         where: {
//             staffId_jobDescriptionId: {
//                 staffId: parseInt(id, 10),
//                 jobDescriptionId: parseInt(jobDescriptionId, 10) // You need the old jobDescription ID
//             }
//         }
//     });

//     if (staffJobDesc) {
//         await prisma.staffJobDescription.update({
//             where: {
//                 id: staffJobDesc.id
//             },
//             data: {
//                 jobDescriptionId: parseInt(jobDescriptionId, 10)
//             }
//         });
//     }

//     return updatedStaff;
// };

export const updateStaff = async (id, data) => {
    const { jobDescriptionId, ...staffData } = data;
    const staffId = parseInt(id, 10);

    // Update staff data
    const updatedStaff = await prisma.staff.update({
        where: { staffId: staffId },
        data: {
            ...staffData,
            updatedAt: new Date(),
        },
    });

    if (jobDescriptionId) {
        const newJobDescId = parseInt(jobDescriptionId, 10);

        // Check if the new job description ID exists
        const jobDescriptionExists = await prisma.jobDescription.findUnique({
            where: {
                jobDescriptionId: newJobDescId,
            }
        });

        if (!jobDescriptionExists) {
            throw new Error(`JobDescription with ID ${newJobDescId} does not exist`);
        }

        // Find the current StaffJobDescription entry for the staff
        const staffJobDesc = await prisma.staffJobDescription.findFirst({
            where: {
                staffId: staffId,
            }
        });

        if (staffJobDesc) {
            // Check if the job description needs to be updated
            if (staffJobDesc.jobDescriptionId !== newJobDescId) {
                await prisma.staffJobDescription.update({
                    where: {
                        id: staffJobDesc.id,
                    },
                    data: {
                        jobDescriptionId: newJobDescId,
                    }
                });
            }
        } else {
            // If there's no current job description entry, create a new one
            await prisma.staffJobDescription.create({
                data: {
                    staffId: staffId,
                    jobDescriptionId: newJobDescId,
                }
            });
        }
    }

    return updatedStaff;
};



export const deleteStaff = async (id) => {
    try {
        // Delete from StaffJobDescription table first
        await prisma.staffJobDescription.deleteMany({
            where: {
                staffId: Number(id)
            }
        });

        // Then delete the staff itself
        const deletedStaff = await prisma.staff.delete({
            where: {
                staffId: Number(id)
            }
        });

        return deletedStaff;
    } catch (error) {
        throw new Error(`Failed to delete staff with ID ${id}: ${error.message}`);
    }
};