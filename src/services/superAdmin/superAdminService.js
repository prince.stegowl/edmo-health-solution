import prisma from '../../prisma.js';

export const getSuperAdminById = async (superAdminId) => {
  return await prisma.superAdmin.findUnique({
    where: { superAdminId },
  });
};

export const updateSuperAdmin = async (superAdminId, data) => {
  return await prisma.superAdmin.update({
    where: { superAdminId },
    data,
  });
};

// module.exports = {
//   getSuperAdminById,
//   updateSuperAdmin,
// };
