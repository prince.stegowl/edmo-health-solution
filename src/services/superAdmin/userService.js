import prisma from '../../prisma.js';

//working
// export const getAllUsersService = async () => {
//     return await prisma.user.findMany({
//         include: {
//             userOrganizations: {
//                 include: {
//                     organization: true
//                 }
//             }
//         }
        
//     });
// };

//working with pagination
export const getAllUsersService = async (page, pageSize) => {
    const skip = (page - 1) * pageSize;
    const take = pageSize;
    
    const users = await prisma.user.findMany({
        skip: skip,
        take: take,
        include: {
            userOrganizations: {
                include: {
                    organization: true
                }
            }
        }
    });

    // Get the total number of users to calculate total pages
    const totalUsers = await prisma.user.count();
    const totalPages = Math.ceil(totalUsers / pageSize);

    return { users, totalPages, currentPage: page };
};

//WORKING PLAN ISSUE
// export const getUserByIdService = async (id) => {
//     // Fetch the user details, including user organizations
//     const user = await prisma.user.findUnique({
//         where: { userId: parseInt(id, 10) },
//         include: {
//             userOrganizations: {
//                 include: {
//                     organization: true
//                 }
//             }
//         }
//     });

//     if (!user) {
//         throw new Error('User not found');
//     }

//     // Fetch the plan details using the plan field from the user object
//     const plan = await prisma.plan.findUnique({
//         where: { planId: user.plan }
//     });

//     // Combine user details and plan details into a single response object
//     return {
//         ...user,
//         plan: plan
//     };
// };
export const getUserByIdService = async (id) => {
    // Fetch the user details, including user organizations
    const user = await prisma.user.findUnique({
        where: { userId: parseInt(id, 10) },
        include: {
            userOrganizations: {
                include: {
                    organization: true
                }
            }
        }
    });

    if (!user) {
        throw new Error('User not found');
    }

    // Initialize the response with user details
    const response = {
        ...user
    };

    // Fetch the plan details using the plan field from the user object
    if (user.plan) {
        const plan = await prisma.plan.findUnique({
            where: { planId: user.plan }
        });

        // Only include the plan details if they exist
        if (plan) {
            response.plan = plan;
        }
    }

    return response;
};

export const createUserService = async (data) => {
    const { organizationId, ...userData } = data;

    // Check if the organizationId exists in the Organization table
    const existingOrganization = await prisma.organization.findUnique({
        where: {
            organizationId: parseInt(organizationId, 10),
        },
    });

    if (!existingOrganization) {
        throw new Error(`Organization with id ${organizationId} does not exist.`);
    }

    // Create user
    const createdUser = await prisma.user.create({
        data: {
            ...userData,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
    });

    // Create entry in the UserOrganization table
    if (organizationId) {
        await prisma.userOrganization.create({
            data: {
                userId: createdUser.userId,
                organizationId: parseInt(organizationId, 10),
            },
        });
    }

    return createdUser;
};

export const updateUserService = async (id, data) => {
    const { organizationId, ...userData } = data;
    const userId = parseInt(id, 10);

    // Update user data
    const updatedUser = await prisma.user.update({
        where: { userId: userId },
        data: {
            ...userData,
            updatedAt: new Date(),
        },
    });

    if (organizationId) {
        const newOrgId = parseInt(organizationId, 10);

        // Check if the new organization ID exists
        const organizationExists = await prisma.organization.findUnique({
            where: {
                organizationId: newOrgId,
            }
        });

        if (!organizationExists) {
            throw new Error(`Organization with ID ${newOrgId} does not exist`);
        }

        // Find the current UserOrganization entry for the user
        const userOrg = await prisma.userOrganization.findFirst({
            where: {
                userId: userId,
            }
        });

        if (userOrg) {
            // Check if the organization needs to be updated
            if (userOrg.organizationId !== newOrgId) {
                await prisma.userOrganization.update({
                    where: {
                        userId_organizationId: {
                            userId: userId,
                            organizationId: userOrg.organizationId,
                        }
                    },
                    data: {
                        organizationId: newOrgId,
                    }
                });
            }
        } else {
            // If there's no current organization entry, create a new one
            await prisma.userOrganization.create({
                data: {
                    userId: userId,
                    organizationId: newOrgId,
                }
            });
        }
    }

    return updatedUser;
};


export const deleteUserService = async (id) => {
    const userId = parseInt(id, 10);

    // Delete the associated entry in the UserOrganization table
    await prisma.userOrganization.deleteMany({
        where: { userId: userId }
    });

    await prisma.userQuestionnaire.deleteMany({
        where: { userId: userId }
    });

    // Delete the user
    const deletedUser = await prisma.user.delete({
        where: { userId: userId },
    });

    return deletedUser;
};