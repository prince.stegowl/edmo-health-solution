import prisma from '../../prisma.js';

export const getAllQuestionnaires = async () => {
    return await prisma.questionnaire.findMany();
};

export const getQuestionnaireById = async (id) => {
    return await prisma.questionnaire.findUnique({ where: { questionnaireId: Number(id) } });
};

export const createQuestionnaire = async (questionnaireData) => {
    return await prisma.questionnaire.create({
        data: {
            ...questionnaireData,
            createdAt: new Date(),
        }
    });
};

export const updateQuestionnaire = async (id, questionnaireData) => {
    return await prisma.questionnaire.update({
        where: { questionnaireId: Number(id) },
        data: {
            ...questionnaireData,
            updatedAt: new Date(),
        },
    });
};

export const deleteQuestionnaire = async (id) => {
    return await prisma.questionnaire.delete({ where: { questionnaireId: Number(id) } });
};