import prisma from '../../prisma.js'

export const getAllOrganizations = async () => {
    return await prisma.organization.findMany();
};

export const getOrganizationById = async (id) => {
    return await prisma.organization.findUnique({ where: { organizationId: Number(id) } });
};

export const createOrganization = async (organizationData) => {
    return await prisma.organization.create({
        data: {
            ...organizationData,
            createdAt: new Date(),
        }
    });
};

export const updateOrganization = async (id, organizationData) => {
    return await prisma.organization.update({
        where: { organizationId: Number(id) },
        data: {
            ...organizationData,
            updatedAt: new Date(),
        },
    });
};


// export const deleteOrganization = async (id) => {
//     // Delete the associated entries in the UserOrganization table
//     await prisma.userOrganization.deleteMany({
//         where: { organizationId: parseInt(id, 10) }
//     });

//     // Delete the organization
//     const deletedOrganization = await prisma.organization.delete({
//         where: { organizationId: parseInt(id, 10) }
//     });

//     return deletedOrganization;
// };


export const deleteOrganization = async (id) => {
    // Update the associated entries in the UserOrganization table to the default organizationId
    await prisma.userOrganization.updateMany({
        where: { organizationId: parseInt(id, 10) },
        data: { organizationId: 1 } // Set to the default organizationId
    });

    await prisma.healthExpertOrganization.updateMany({
        where: { organizationId: parseInt(id, 10) },
        data: { organizationId: 1 } // Set to the default organizationId
    });

    // Delete the organization
    const deletedOrganization = await prisma.organization.delete({
        where: { organizationId: parseInt(id, 10) }
    });

    return deletedOrganization;
};