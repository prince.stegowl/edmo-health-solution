// import prisma from '../../prisma.js'

// export const getAllHealthExperts = async () => {
//     return await prisma.healthExpert.findMany();
// };

// export const getHealthExpertById = async (id) => {
//     return await prisma.healthExpert.findUnique({ where: { healthExpertId: Number(id) } });
// };

// export const createHealthExpert = async (healthExpertData) => {
//     return await prisma.healthExpert.create({
//         data: {
//             ...healthExpertData,
//             createdAt: new Date(),
//         }
//     });
// };

// export const updateHealthExpert = async (id, healthExpertData) => {
//     return await prisma.healthExpert.update({
//         where: { healthExpertId: Number(id) },
//         data: {
//             ...healthExpertData,
//             updatedAt: new Date(),
//         },
//     });
// };

// export const deleteHealthExpert = async (id) => {
//     return await prisma.healthExpert.delete({ where: { healthExpertId: Number(id) } });
// };



import prisma from '../../prisma.js';

// Create a HealthExpert
export const createHealthExpert = async (data) => {
    const { organizationId, ...healthExpertData } = data;

    // Create healthExpert and associate with organization if organizationId is provided
    const createdHealthExpert = await prisma.healthExpert.create({
        data: {
            ...healthExpertData,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
    });

    // Create entry in the HealthExpertOrganization table
    if (organizationId) {
        await prisma.healthExpertOrganization.create({
            data: {
                healthExpertId: createdHealthExpert.healthExpertId,
                organizationId: parseInt(organizationId, 10)
            }
        });
    }

    return createdHealthExpert;
};

// Get HealthExpert by ID
export const getHealthExpertById = async (id) => {
    const healthExpert = await prisma.healthExpert.findUnique({
        where: { healthExpertId: parseInt(id, 10) },
        include: {
            healthExpertOrganizations: {
                include: {
                    Organization: true
                }
            }
        }
    });

    return {
        ...healthExpert,
    };
};

// export const getAllUsersService = async () => {
//     return await prisma.user.findMany({
//         include: {
//             userOrganizations: {
//                 include: {
//                     organization: true
//                 }
//             }
//         }
        
//     });
// };

//Get all HealthExperts
export const getAllHealthExperts = async () => {
    const healthExperts = await prisma.healthExpert.findMany({
        include: {
            healthExpertOrganizations: {
                include: {
                    Organization: true
                }
            }
        }
    });

    return healthExperts.map(healthExpert => ({
        ...healthExpert,
       
    }));
};

// export const updateHealthExpert = async (id, data) => {
//     const { organizationId, ...healthExpertData } = data;

//     // Update healthExpert data
//     const updatedHealthExpert = await prisma.healthExpert.update({
//         where: { healthExpertId: parseInt(id, 10) },
//         data: {
//             ...healthExpertData,
//             updatedAt: new Date(),
//         },
//     });

//     const existingRelation = await prisma.healthExpertOrganization.findUnique({
//         where: {
//             healthExpertId_organizationId: {
//                 healthExpertId: parseInt(id, 10),
//                 organizationId: parseInt(organizationId, 10), // Existing organizationId in the relation
//             },
//         },
//     });

//     if (existingRelation) {
//         // Update the existing relation with the new organizationId
//         await prisma.healthExpertOrganization.update({
//             where: {
//                 id: existingRelation.id,
//             },
//             data: {
//                 organizationId: parseInt(organizationId, 10),
//             },
//         });
//     } else {
//         // If the relation does not exist, create a new one
//         await prisma.healthExpertOrganization.create({
//             data: {
//                 healthExpertId: parseInt(id, 10),
//                 organizationId: parseInt(organizationId, 10),
//             },
//         });
//     }

//     return updatedHealthExpert;
// };



export const updateHealthExpert = async (id, data) => {
    const { organizationId, ...healthExpertData } = data;
    const healthExpertId = parseInt(id, 10);

    // Update healthExpert data
    const updatedHealthExpert = await prisma.healthExpert.update({
        where: { healthExpertId: healthExpertId },
        data: {
            ...healthExpertData,
            updatedAt: new Date(),
        },
    });

    if (organizationId) {
        const newOrgId = parseInt(organizationId, 10);

        // Check if the new organization ID exists
        const organizationExists = await prisma.organization.findUnique({
            where: {
                organizationId: newOrgId,
            }
        });

        if (!organizationExists) {
            throw new Error(`Organization with ID ${newOrgId} does not exist`);
        }

        // Find the current HealthExpertOrganization entry for the health expert
        const healthExpertOrg = await prisma.healthExpertOrganization.findFirst({
            where: {
                healthExpertId: healthExpertId,
            }
        });

        if (healthExpertOrg) {
            // Check if the organization needs to be updated
            if (healthExpertOrg.organizationId !== newOrgId) {
                await prisma.healthExpertOrganization.update({
                    where: {
                        healthExpertId_organizationId: {
                            healthExpertId: healthExpertId,
                            organizationId: healthExpertOrg.organizationId,
                        }
                    },
                    data: {
                        organizationId: newOrgId,
                    }
                });
            }
        } else {
            // If there's no current organization entry, create a new one
            await prisma.healthExpertOrganization.create({
                data: {
                    healthExpertId: healthExpertId,
                    organizationId: newOrgId,
                }
            });
        }
    }

    return updatedHealthExpert;
};





// Delete a HealthExpert
export const deleteHealthExpert = async (id) => {
    // Delete the associated entry in the HealthExpertOrganization table
    await prisma.healthExpertOrganization.deleteMany({
        where: { healthExpertId: parseInt(id, 10) }
    });

    // Delete the healthExpert
    const deletedHealthExpert = await prisma.healthExpert.delete({
        where: { healthExpertId: parseInt(id, 10) },
    });

    return deletedHealthExpert;
};
