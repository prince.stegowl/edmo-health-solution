import prisma from '../../prisma.js'

export const getAllJobDescriptionsService = async () => {
    return await prisma.jobDescription.findMany();
};

export const getJobDescriptionByIdService = async (id) => {
    return await prisma.jobDescription.findUnique({
        where: { jobDescriptionId: parseInt(id) },
    });
};

export const createJobDescriptionService = async (data) => {
    return await prisma.jobDescription.create({
        data,
    });
};

export const updateJobDescriptionService = async (id, data) => {
    return await prisma.jobDescription.update({
        where: { jobDescriptionId: parseInt(id) },
        data,
    });
};

// export const deleteJobDescriptionService = async (id) => {
//     return await prisma.jobDescription.delete({
//         where: { jobDescriptionId: parseInt(id) },
//     });
// };

export const deleteJobDescriptionService = async (id) => {
    const jobDescriptionId = parseInt(id);

    // Delete all entries in the StaffJobDescription table for the given jobDescriptionId
    await prisma.staffJobDescription.deleteMany({
        where: { jobDescriptionId: jobDescriptionId },
    });

    // Delete the job description
    return await prisma.jobDescription.delete({
        where: { jobDescriptionId: jobDescriptionId },
    });
};