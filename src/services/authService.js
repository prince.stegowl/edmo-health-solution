import prisma from '../prisma.js';
import { generateOTP } from '../utils/otpUtils.js';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

const modelMapping = {
  Users: prisma.user,
  SuperAdmin: prisma.superAdmin,
  Staff: prisma.staff,
  HealthExpert: prisma.healthExpert,
  Organisation: prisma.organization,
};

export const createOrUpdateUserOTP = async (userType, userNumber) => {
  const Model = modelMapping[userType];
  if (!Model) {
    throw new Error(`Invalid user type: ${userType}`);
  }

  let user = await Model.findFirst({
    where: { userNumber: userNumber },
  });

  if (!user) {
    user = await Model.create({
      data: {
        userNumber: userNumber,
        otp: generateOTP(),
        otpCreatedAt: new Date(),
      },
    });
  } else {
    user = await Model.update({
      where: { [Object.keys(user).find(key => key.endsWith('Id'))]: user[Object.keys(user).find(key => key.endsWith('Id'))] },
      data: {
        otp: generateOTP(),
        otpCreatedAt: new Date(),
      },
    });
  }

  return user;
};


// export const createOrUpdateUserOTP = async (userType, userNumber) => {
//   console.log(userType,"this is the userType");
//   const Model = modelMapping[userType];
//   if (!Model) {
//     throw new Error(`Invalid user type: ${userType}`);
//   }

//   // Handle the "Users" type differently to allow new numbers
//   if (userType === 'Users') {
//     let user = await Model.findFirst({
//       where: { userNumber: userNumber },
//     });

//     if (!user) {
//       // Create a new user if not found
//       user = await Model.create({
//         data: {
//           userNumber: userNumber,
//           otp: generateOTP(),
//           otpCreatedAt: new Date(),
//         },
//       });
//     } else {
//       // Update the existing user
//       user = await Model.update({
//         where: { id: user.id },
//         data: {
//           otp: generateOTP(),
//           otpCreatedAt: new Date(),
//         },
//       });
//     }

//     return user;
//   }

//   // For other user types, handle as before
//   let user = await Model.findFirst({
//     where: { userNumber: userNumber },
//   });

//   if (!user) {
//     // For other user types, do not create a new user if not found
//     throw new Error(`User with number ${userNumber} not found for user type ${userType}`);
//   } else {
//     // Update the existing user
//     user = await Model.update({
//       where: { [Object.keys(user).find(key => key.endsWith('Id'))]: user[Object.keys(user).find(key => key.endsWith('Id'))] },
//       data: {
//         otp: generateOTP(),
//         otpCreatedAt: new Date(),
//       },
//     });
//   }

//   return user;
// };

export const verifyUserOTP = async (userType, userNumber, otp) => {
  const Model = modelMapping[userType];
  if (!Model) {
    throw new Error(`Invalid user type: ${userType}`);
  }

  const user = await Model.findFirst({
    where: {
      userNumber: userNumber,
      otp,
    },
  });

  if (!user) return { error: 'Invalid OTP' };

  const otpExpirationTime = new Date(user.otpCreatedAt);
  otpExpirationTime.setSeconds(otpExpirationTime.getSeconds() + 90);

  if (new Date() > otpExpirationTime) {
    return { error: 'OTP has expired' };
  }

  await Model.update({
    where: { [Object.keys(user).find(key => key.endsWith('Id'))]: user[Object.keys(user).find(key => key.endsWith('Id'))] },
    data: { isVerified: true },
  });

  const token = jwt.sign(
    {
      userId: user[Object.keys(user).find(key => key.endsWith('Id'))],
      userType: userType,
    },
    process.env.JWT_SECRET,
    { expiresIn: '1h' }
  );

  return { message: 'OTP verified successfully', token, userId: user[Object.keys(user).find(key => key.endsWith('Id'))] };
};
