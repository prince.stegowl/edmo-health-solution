import prisma from '../../prisma.js';

export const getUsersByOrganizationId = async (organizationId) => {
  return await prisma.user.findMany({
    where: {
      userOrganizations: {
        some: {
          organizationId: parseInt(organizationId),
        },
      },
    },
  });
};
