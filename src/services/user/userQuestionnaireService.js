import prisma from '../../prisma.js';

const createUserQuestionnaire = async (data) => {
  return prisma.userQuestionnaire.create({ data });
};

const getAllUserQuestionnaires = async () => {
  return prisma.userQuestionnaire.findMany();
};

const getUserQuestionnaireById = async (id) => {
  return prisma.userQuestionnaire.findUnique({ where: { userQuestionnaireId: parseInt(id) } });
};

const getUserQuestionnairesByUserIdService = async (userId) => {
  // Convert userId to integer if necessary
  const id = parseInt(userId, 10);

  // Fetch user questionnaires by userId
  const userQuestionnaires = await prisma.userQuestionnaire.findMany({
      where: { userId: id },
      include: {
          questionnaire: true // Include related questionnaire details
      }
  });

  // Handle case where no user questionnaires are found
  if (!userQuestionnaires || userQuestionnaires.length === 0) {
      throw new Error('No questionnaires found for this user');
  }

  return userQuestionnaires;
};

const updateUserQuestionnaire = async (id, data) => {
  return prisma.userQuestionnaire.update({
    where: { userQuestionnaireId: parseInt(id) },
    data,
  });
};

const deleteUserQuestionnaire = async (id) => {
  return prisma.userQuestionnaire.delete({ where: { userQuestionnaireId: parseInt(id) } });
};

export default {
  createUserQuestionnaire,
  getAllUserQuestionnaires,
  getUserQuestionnaireById,
  updateUserQuestionnaire,
  deleteUserQuestionnaire,
  getUserQuestionnairesByUserIdService
};
