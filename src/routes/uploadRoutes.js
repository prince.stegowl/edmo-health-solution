import express from 'express';
import multer from 'multer';
import { uploadFile, deleteFile, listFiles } from '../controllers/uploadController.js';
import verifyToken from '../middlewares/authMiddleware.js';
// import { uploadSingle } from '../middlewares/uploadMiddleware.js';
import { validateUploadFile, validateUpdateFile, validateDeleteFile } from '../validators/uploadValidator.js';

const router = express.Router();
const upload = multer({ storage: multer.memoryStorage() });


router.post('/post', uploadFile);
router.get('/files', listFiles);
router.delete('/delete', deleteFile);


// router.post('/post', verifyToken, uploadSingle, uploadFile);
// router.delete('/delete', verifyToken, deleteFile);
// router.get('/files', verifyToken, listFiles);



export default router;
