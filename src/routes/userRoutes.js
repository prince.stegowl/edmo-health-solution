import express from 'express';
import { getAllUsers, getUserById, createUser, updateUser, deleteUser } from '../controllers/superAdmin/userController.js';
import { validateUser,validateUserId } from '../validators/userValidator.js';
import verifyToken from '../middlewares/authMiddleware.js';

const router = express.Router();

// router.get('/get', verifyToken, validateUser, getAllUsers);
// router.get('/get/:id', verifyToken, validateUser, getUserById);
// router.post('/post', verifyToken, validateUser, createUser);
// router.put('/update/:id', verifyToken, validateUser, updateUser);
// router.delete('/remove/:id', verifyToken, validateUser, deleteUser);



router.get('/get', getAllUsers);
router.get('/get/:id', validateUserId, getUserById);
router.post('/post', createUser);
router.put('/update/:id', validateUserId, validateUser, updateUser);
router.delete('/remove/:id', verifyToken, validateUserId, deleteUser);

export default router;
