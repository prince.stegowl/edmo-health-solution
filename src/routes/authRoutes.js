import express from 'express'
import {loginUser, verifyOTP} from '../controllers/authcontroller.js'
import verifyToken from '../middlewares/authMiddleware.js';

const router = express.Router();

router.post('/login', loginUser);
router.post('/verify-otp', verifyOTP);
router.post('/verify-TOken', verifyToken)

export default router;

