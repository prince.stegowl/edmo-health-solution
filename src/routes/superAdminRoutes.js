import express from 'express'
import {getSuperAdminById, updateSuperAdminById} from '../controllers/superAdmin/superAdminController.js'
import verifyToken from '../middlewares/authMiddleware.js'
import {validateSuperAdminUpdate} from '../validators/superAdminValidator.js'
import upload, { uploadFileToS3 } from '../middlewares/uploadMiddleware.js'
import uploadSingle from '../middlewares/uploadMiddleware.js'

const router = express.Router();

router.get('/profile/:id', verifyToken, getSuperAdminById);
router.put('/profile/:id', verifyToken, updateSuperAdminById);
// router.put('/profile/:id', verifyToken, uploadSingle, validateSuperAdminUpdate, updateSuperAdminById);

// module.exports = router;
export default router





