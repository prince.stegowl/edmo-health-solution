import express from 'express'
import superAdminRoutes from './superAdminRoutes.js'
import authRoutes from './authRoutes.js'
import plansRoutes from './plansRoutes.js'
import uploadRoutes from './uploadRoutes.js'
import staffRoutes from './staffRoutes.js'
import organizationRoutes from './organizationRoutes.js'
import healthExpertRoutes from './healthExpertRoutes.js'
import questionnaireRoutes from './questionnaireRoutes.js'
import usersRoutes from './userRoutes.js'
import jobDescriptionRoutes from './jobDescriptionRoutes.js'
import userQuestionnaireRoutes from './userQuestionnaireRoutes.js'
import organizationUsersRoutes from './organizationUsersRoutes.js'

const app = express.Router();

app.use('/auth', authRoutes);
app.use('/superadmin', superAdminRoutes);
app.use('/plans', plansRoutes);
app.use('/upload', uploadRoutes);
app.use('/staff', staffRoutes);
app.use('/organization', organizationRoutes);
app.use('/healthexpert', healthExpertRoutes);
app.use('/questionnaire', questionnaireRoutes);
app.use('/users', usersRoutes);
app.use('/jobdescription', jobDescriptionRoutes);
app.use('/userquestionnaire', userQuestionnaireRoutes);
app.use('/organizationusers', organizationUsersRoutes);


// app.post('/upload', uploadMiddleware, processFormData);

export default app;

