import { Router } from 'express';
import userQuestionnaireController from '../controllers/user/userQuestionnaireController.js';

const router = Router();


router.get('/get', userQuestionnaireController.getAllUserQuestionnaires);
router.get('/get/:id', userQuestionnaireController.getUserQuestionnaireById);
router.get('/getuser/:id', userQuestionnaireController.getUserQuestionnairesByUserId);
router.post('/post/', userQuestionnaireController.createUserQuestionnaire);
router.put('/update/:id', userQuestionnaireController.updateUserQuestionnaire);
router.delete('/remove/:id', userQuestionnaireController.deleteUserQuestionnaire);

export default router;