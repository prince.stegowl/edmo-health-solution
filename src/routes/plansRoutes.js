// import express from 'express';
// import { fetchAllPlans, addNewPlan, modifyPlan, removePlan, fetchPlanById } from '../controllers/plansController.js';
// import verifyToken from '../middlewares/authMiddleware.js';
// // import { uploadFieldsMiddleware } from '../middlewares/uploadMiddleware.js';
// import { planValidationRules } from '../validators/plansValidator.js';

// const router = express.Router();

// router.get('/get', fetchAllPlans);
// router.get('/get/:id', fetchPlanById);
// router.post('/create', verifyToken, planValidationRules, addNewPlan);
// router.put('/:id', verifyToken, modifyPlan);
// // router.delete('/:id', verifyToken, removePlan);

// export default router;




import express from 'express';
import {addNewPlan,getAllPlans,getSinglePlanById,updatePlan,updatePlanStatus} from '../controllers/superAdmin/plansController.js';
import { validatePlan, validatePlanId } from '../validators/plansValidator.js';
import multer from 'multer';
import verifyToken from '../middlewares/authMiddleware.js';

const upload = multer(); // Adjust multer settings as needed
const router = express.Router();

router.post('/create',verifyToken, validatePlan, addNewPlan);
router.get('/get',verifyToken, getAllPlans);
router.get('/gets/:id',verifyToken, getSinglePlanById);
router.put('/update/:id',verifyToken, updatePlan);
// router.delete('/remove/:id',verifyToken, validatePlanId, deletePlan);
router.delete('/status/:id',verifyToken, validatePlanId, updatePlanStatus); // Disable

export default router;