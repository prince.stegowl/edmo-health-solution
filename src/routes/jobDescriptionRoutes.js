import express from 'express';
import { getAllJobDescriptions, getJobDescriptionById, createJobDescription, updateJobDescription, deleteJobDescription } from '../controllers/superAdmin/jobDescriptionController.js';
import { jobDescriptionValidationRules } from '../validators/jobDescriptionValidatior.js';

const router = express.Router();

router.get('/get', getAllJobDescriptions);
router.get('/get/:id', getJobDescriptionById);
router.post('/post', jobDescriptionValidationRules(), createJobDescription);
router.put('/update/:id', jobDescriptionValidationRules(), updateJobDescription);
router.delete('/remove/:id', deleteJobDescription);

export default router;
