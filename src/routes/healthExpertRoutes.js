import { Router } from 'express';
import { getAllHealthExpertsHandler, getHealthExpertByIdHandler, createHealthExpertHandler, updateHealthExpertHandler, deleteHealthExpertHandler } from '../../src/controllers/superAdmin/healthExpertController.js';
import { validateHealthExpert, validateHealthExpertId } from '../../src/validators/healthExpertValidator.js';
import verifyToken from '../middlewares/authMiddleware.js';

const router = Router();

// router.get('/get',verifyToken, getAllHealthExpertsHandler);
// router.get('/get/:id',verifyToken, validateHealthExpertId, getHealthExpertByIdHandler);
// router.post('/post',verifyToken,validateHealthExpert, createHealthExpertHandler);
// router.put('/update/:id',verifyToken, validateHealthExpertId, validateHealthExpert, updateHealthExpertHandler);
// router.delete('/remove/:id',verifyToken, validateHealthExpertId, deleteHealthExpertHandler);


router.get('/get', verifyToken, getAllHealthExpertsHandler);
router.get('/get/:id', verifyToken, validateHealthExpertId, getHealthExpertByIdHandler);
router.post('/post', verifyToken, validateHealthExpert, createHealthExpertHandler);
router.put('/update/:id', verifyToken, validateHealthExpertId, validateHealthExpert, updateHealthExpertHandler);
router.delete('/remove/:id', verifyToken, deleteHealthExpertHandler);


export default router;
