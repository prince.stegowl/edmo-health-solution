import { Router } from 'express';
import { getAllQuestionnairesHandler, getQuestionnaireByIdHandler, createQuestionnaireHandler, updateQuestionnaireHandler, deleteQuestionnaireHandler } from '../../src/controllers/superAdmin/questionnaireController.js';
import { validateQuestionnaire, validateQuestionnaireId } from '../../src/validators/questionnaireValidator.js';
import verifyToken from '../middlewares/authMiddleware.js';

const router = Router();

router.get('/get', getAllQuestionnairesHandler);
router.get('/get/:id',verifyToken, validateQuestionnaireId, getQuestionnaireByIdHandler);
router.post('/post',verifyToken,validateQuestionnaire, createQuestionnaireHandler);
router.put('/update/:id',verifyToken,validateQuestionnaire, updateQuestionnaireHandler);
router.delete('/remove/:id',verifyToken, validateQuestionnaireId, deleteQuestionnaireHandler);

export default router;
