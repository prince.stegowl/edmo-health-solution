import express from 'express'
import { getAllStaffHandler, getStaffByIdHandler, createStaffHandler, updateStaffHandler, deleteStaffHandler } from '../../src/controllers/superAdmin/staffController.js';
import { validateStaff, validateStaffId } from '../../src/validators/staffValidator.js';
import verifyToken from '../middlewares/authMiddleware.js';

const router = express.Router();

router.get('/get', getAllStaffHandler);
router.get('/get/:id', validateStaffId, getStaffByIdHandler);
router.post('/post/',verifyToken, validateStaff, createStaffHandler);
router.put('/update/:id',verifyToken, updateStaffHandler);
router.delete('/delete/:id',verifyToken, validateStaffId, deleteStaffHandler);


export default router;
