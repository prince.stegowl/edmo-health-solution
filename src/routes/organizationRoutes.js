import { Router } from 'express';
import { getAllOrganizationsHandler, getOrganizationByIdHandler, createOrganizationHandler, updateOrganizationHandler, deleteOrganizationHandler } from '../../src/controllers/superAdmin/organizationController.js';
import { validateOrganization, validateOrganizationId } from '../../src/validators/organizationValidator.js';
import verifyToken from '../middlewares/authMiddleware.js';

const router = Router();

router.get('/get', getAllOrganizationsHandler);
router.get('/get/:id', validateOrganizationId, getOrganizationByIdHandler);
router.post('/post', validateOrganization, createOrganizationHandler);
router.put('/update/:id', updateOrganizationHandler);
router.delete('/remove/:id', validateOrganizationId, deleteOrganizationHandler);

export default router;
