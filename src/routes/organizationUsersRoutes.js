import express from 'express';
import { getUsersByOrganizationIdController } from '../controllers/organization/organizationUsersController.js';

const router = express.Router();

router.get('/get/:organizationId', getUsersByOrganizationIdController);

export default router;
