import { body, param } from 'express-validator';

export const validateQuestionnaire = [
    body('question').isString().withMessage('Question must be a string').notEmpty().withMessage('Question is required'),
    body('dataType').isString().withMessage('Data Type must be a string').notEmpty().withMessage('Data Type is required'),
    body('mandatory').isBoolean().withMessage('Mandatory must be a boolean').notEmpty().withMessage('Mandatory is required')
];

export const validateQuestionnaireId = [
    param('id').isNumeric().withMessage('ID must be a number')
];
