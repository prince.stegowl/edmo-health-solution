// const { body, validationResult } = require('express-validator');
import {body, validationResult} from 'express-validator'

// Validation rules for updating a SuperAdmin
export const validateSuperAdminUpdate = [
  // body('superAdminName').notEmpty().withMessage('Name is required'),
  body('superAdminName')
    .notEmpty().withMessage('Name is required')
    .matches(/^[a-zA-Z\s]+$/).withMessage('Name should only contain letters and spaces'),
  body('superAdminEmail').isEmail().withMessage('Invalid email address'),
  body('userNumber').isLength({ min: 10, max: 15 }).withMessage('Invalid phone number'),
  
  // Add more validations as needed
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

export default validateSuperAdminUpdate;
