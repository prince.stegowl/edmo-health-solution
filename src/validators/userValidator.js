

// export const validateUser = (user) => {
//     // const schema = Joi.object({
//     //     userNumber: Joi.string().required(),
//     //     otp: Joi.number().optional(),
//     //     otpCreatedAt: Joi.date().optional(),
//     //     isVerified: Joi.boolean().optional(),
//     //     firstName: Joi.string().optional(),
//     //     lastName: Joi.string().optional(),
//     //     organization: Joi.string().optional(),
//     //     address: Joi.string().optional(),
//     //     city: Joi.string().optional(),
//     //     state: Joi.string().optional(),
//     //     pincode: Joi.string().optional(),
//     //     email: Joi.string().optional(),
//     //     gender: Joi.string().optional(),
//     //     DOB: Joi.string().optional(),
//     //     plan: Joi.string().optional(),
//     //     height: Joi.string().optional(),
//     //     weight: Joi.string().optional(),
//     // });
//     return null
// };


import { body, param } from 'express-validator';

export const validateUser = [
    body('firstName').optional().isString().withMessage('First name must be a string'),
    body('lastName').optional().isString().withMessage('Last name must be a string'),
    body('organization').optional().isString().withMessage('Organization must be a string'),
    body('address').optional().isString().withMessage('Address must be a string'),
    body('city').optional().isString().withMessage('City must be a string'),
    body('state').optional().isString().withMessage('State must be a string'),
    body('pincode').optional().isString().withMessage('Pincode must be a string'),
    body('email').optional().isEmail().withMessage('Email must be a valid email address'),
    body('gender').optional().isString().withMessage('Gender must be a string'),
    body('DOB').optional().isISO8601().withMessage('DOB must be a valid date'),
    body('height').optional().isString().withMessage('Height must be a string'),
    body('weight').optional().isString().withMessage('Weight must be a string'),
    body('paymentStatus').optional().isString().withMessage('Payment status must be a string')

];

export const validateUserId = [
    param('id').isNumeric().withMessage('ID must be a number')
];