import { body, param } from 'express-validator';

export const validatePlan = [
  body('planName').notEmpty().withMessage('Plan name is required'),
  body('planPrice').isInt().withMessage('Plan price must be an integer'),
  body('planDuration').notEmpty().withMessage('Plan duration is required'),
  body('planDescription').notEmpty().withMessage('Plan description is required'),
  body('healthExpertAppointments').isInt().withMessage('Health expert appointments must be an integer'),
  body('doctorAppointments').isInt().withMessage('Doctor appointments must be an integer'),
];

export const validatePlanId = [
  param('id').isInt().withMessage('Plan ID must be an integer'),
];

