import { body } from 'express-validator';

export const jobDescriptionValidationRules = () => {
    return [
        body('jobDescription').notEmpty().withMessage('Job description is required'),
    ];
};
