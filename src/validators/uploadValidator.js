import { body } from 'express-validator';

export const validateUploadFile = [
  body('file').exists().withMessage('File is required'),
];

export const validateUpdateFile = [
  body('file').exists().withMessage('File is required'),
  body('existingKey').exists().withMessage('Existing file key is required'),
];

export const validateDeleteFile = [
  body('fileKey').exists().withMessage('File key is required'),
];
