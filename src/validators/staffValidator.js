import { body, param } from 'express-validator';

export const validateStaff = [
    body('userNumber').isString().withMessage('User number must be a string').notEmpty().withMessage('User number is required'),
    body('isVerified').optional().isBoolean().withMessage('Is Verified must be a boolean'),
    body('firstName').isString().withMessage('First name must be a string').notEmpty().withMessage('First name is required'),
    body('lastName').isString().withMessage('Last name must be a string').notEmpty().withMessage('Last name is required'),
    body('city').isString().withMessage('City must be a string').notEmpty().withMessage('City is required'),
    body('state').isString().withMessage('State must be a string').notEmpty().withMessage('State is required'),
    body('email').isEmail().withMessage('Email must be a valid email address').notEmpty().withMessage('Email is required'),
    body('active').optional().isBoolean().withMessage('Active must be a boolean')
];

export const validateStaffId = [
    param('id').isNumeric().withMessage('ID must be a number')
];
