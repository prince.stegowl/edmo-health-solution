import { body, param } from 'express-validator';

export const validateOrganization = [
    body('userNumber').isString().withMessage('User number must be a string').notEmpty().withMessage('User number is required'),
    body('otp').optional().isNumeric().withMessage('OTP must be a number'),
    body('otpCreatedAt').optional().isISO8601().withMessage('OTP Created At must be a valid date'),
    body('isVerified').optional().isBoolean().withMessage('Is Verified must be a boolean'),
    body('organizationName').optional().isString().withMessage('Organization name must be a string'),
    body('firstName').optional().isString().withMessage('First name must be a string'),
    body('lastName').optional().isString().withMessage('Last name must be a string'),
    body('address').optional().isString().withMessage('Address must be a string'),
    body('city').optional().isString().withMessage('City must be a string'),
    body('state').optional().isString().withMessage('State must be a string'),
    body('pincode').optional().isString().withMessage('Pincode must be a string'),
    body('email').optional().isEmail().withMessage('Email must be a valid email address'),
    body('active').optional().isBoolean().withMessage('Active must be a boolean')
];

export const validateOrganizationId = [
    param('id').isNumeric().withMessage('ID must be a number')
];
