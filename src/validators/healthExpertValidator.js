// import { body, param } from 'express-validator';

// export const validateHealthExpert = [
//     body('userNumber').isString().withMessage('User number must be a string').notEmpty().withMessage('User number is required'),
//     body('isVerified').optional().isBoolean().withMessage('Is Verified must be a boolean'),
//     body('firstName').isString().withMessage('First name must be a string').notEmpty().withMessage('First name is required'),
//     body('lastName').isString().withMessage('Last name must be a string').notEmpty().withMessage('Last name is required'),
//     body('address').isString().withMessage('Address must be a string').notEmpty().withMessage('Address is required'),
//     body('city').isString().withMessage('City must be a string').notEmpty().withMessage('City is required'),
//     body('state').isString().withMessage('State must be a string').notEmpty().withMessage('State is required'),
//     body('pincode').isString().withMessage('Pincode must be a string').notEmpty().withMessage('Pincode is required'),
//     body('email').isEmail().withMessage('Email must be a valid email address').notEmpty().withMessage('Email is required'),
//     body('gender').isString().withMessage('Gender must be a string').notEmpty().withMessage('Gender is required'),
//     body('active').optional().isBoolean().withMessage('Active must be a boolean'),
//     body('qualification').isString().withMessage('qualification must be a string'),
//     body('type').isString().withMessage('qualification must be a string'),
//     body('employment').isString().isBoolean().withMessage('employment must be a string'),
//     body('language').isString().withMessage('language must be a string')
// ];

// export const validateHealthExpertId = [
//     param('id').isNumeric().withMessage('ID must be a number')
// ];

import { body, param } from 'express-validator';

export const validateHealthExpert = [
    body('userNumber').isString().withMessage('User number must be a string').notEmpty().withMessage('User number is required'),
    body('isVerified').optional().isBoolean().withMessage('Is Verified must be a boolean'),
    body('firstName').isString().withMessage('First name must be a string').notEmpty().withMessage('First name is required'),
    body('lastName').isString().withMessage('Last name must be a string').notEmpty().withMessage('Last name is required'),
    body('address').isString().withMessage('Address must be a string').notEmpty().withMessage('Address is required'),
    body('city').isString().withMessage('City must be a string').notEmpty().withMessage('City is required'),
    body('state').isString().withMessage('State must be a string').notEmpty().withMessage('State is required'),
    body('pincode').isString().withMessage('Pincode must be a string').notEmpty().withMessage('Pincode is required'),
    body('email').isEmail().withMessage('Email must be a valid email address').notEmpty().withMessage('Email is required'),
    body('gender').isString().withMessage('Gender must be a string').notEmpty().withMessage('Gender is required'),
    body('active').optional().isBoolean().withMessage('Active must be a boolean'),
    body('qualification').isString().withMessage('Qualification must be a string'),
    body('type').isString().withMessage('Type must be a string'),
    body('employment').isString().withMessage('Employment must be a String'),
    body('language').isString().withMessage('Language must be a string')
];

export const validateHealthExpertId = [
    param('id').isNumeric().withMessage('ID must be a number')
];
