// const jwt = require('jsonwebtoken');
// const dotenv = require('dotenv');

// // Load environment variables from .env file
// dotenv.config();

// const verifyToken = (req, res, next) => {
//   const token = req.headers['authorization'];

//   if (!token) {
//     return res.status(403).json({ error: 'No token provided' });
//   }

//   jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
//     if (err) {
//       return res.status(401).json({ error: 'Failed to authenticate token' });
//     }

//     // Save the decoded user info for use in other routes
//     req.user = decoded;
//     next();
//   });
// };

// module.exports = verifyToken;


// const jwt = require('jsonwebtoken');
// const dotenv = require('dotenv');

// // Load environment variables from .env file
// dotenv.config();

// const verifyToken = (req, res, next) => {
//   const token = req.headers['authorization']?.split(' ')[1]; // Get token from header

//   if (!token) {
//     return res.status(403).json({ error: 'No token provided' });
//   }

//   jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
//     if (err) {
//       return res.status(401).json({ error: 'Failed to authenticate token' });
//     }

//     // Save the decoded user info for use in other routes
//     req.user = decoded;
//     next();
//   });
// };

// module.exports = verifyToken;








// const jwt = require('jsonwebtoken');

// // Middleware to verify JWT token
// const verifyToken = (req, res, next) => {
//   // const token = req.headers['authorization']?.split(' ')[1]; // Get token from header
//   const token = req.headers['authorization']
//   if (!token) {
//     return res.status(403).json({ error: 'No token provided' });
//   }

//   jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
//     if (err) {
//       return res.status(401).json({ error: 'Failed to authenticate token' });
//     }

//     // Save the decoded user info for use in other routes
//     req.user = decoded;
//     next();
//   });
// };

// module.exports = verifyToken;

import jwt from  'jsonwebtoken'

const verifyToken = (req, res, next) => {
  try {
    const authHeader = req.headers['authorization'];

    // Check if the Authorization header is provided
    if (!authHeader) {
      console.error('No Authorization header provided');
      return res.status(403).json({ error: 'No token provided' });
    }

    // Extract the token from the Authorization header
    const token = authHeader.split(' ')[1];

    // Check if the token is properly extracted
    if (!token) {
      console.error('No token found in Authorization header');
      return res.status(403).json({ error: 'No token provided' });
    }

    // Verify the token
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        console.error('Failed to authenticate token:', err);
        return res.status(401).json({ error: 'Failed to authenticate token' });
      }

      // Save the decoded user info for use in other routes
      req.user = decoded;
      next();
    });
  } catch (error) {
    console.error('Error in verifyToken middleware:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

export default verifyToken;

