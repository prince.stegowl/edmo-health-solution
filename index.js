//this one is for the live usage
import express from 'express';
import routes from './src/routes/index.js';
import cors from 'cors';

const app = express();

// Use cors middleware before defining routes
app.use(cors());

// To configure CORS to allow requests from a specific origin
// app.use(cors({ origin: 'http://localhost:5173' }));

app.use(express.json());
app.use('/api', routes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

export default app;




// //for using the api in local network
// import express from 'express';
// import routes from './src/routes/index.js';
// import cors from 'cors';
// import dotenv from 'dotenv';

// dotenv.config();

// const app = express();

// // Use cors middleware before defining routes
// app.use(cors());


// app.use(express.json());
// app.use('/api', routes);

// const PORT = process.env.PORT || 3000;
// const HOST = '192.168.1.75';
// //const HOST = '192.168.1.2';

// app.listen(PORT, HOST, () => {
//     console.log(`Server is running on http://${HOST}:${PORT}`);
// });
